import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm:FormGroup;

  constructor(private fb:FormBuilder,private authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      nombre: ['',Validators.required],
      correo: ['',[Validators.required,Validators.email]],
      password: ['',Validators.required],
    });
  }

  crearUsuario(){
    console.log(this.registerForm);
    console.log(this.registerForm.value);
    if (this.registerForm.invalid) {
      return ;
    }
    this.loading();

    const {nombre,correo,password} = this.registerForm.value;
    this.authService.crearUsuario(nombre,correo,password).then(credenciales=>{
      console.log(credenciales);
      Swal.close();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Has sido logeado',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['/']);
    }).catch(error=>{
      Swal.close();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: error.message,
      })
      console.error(error);
      
    });
  }

  loading() {
    Swal.fire({
      title: 'Espere...',
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }

}
