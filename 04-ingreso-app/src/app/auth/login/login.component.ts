import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }
  logear() {
    console.log(this.loginForm.value);
    if (this.loginForm.invalid) {
      return;
    }
    this.loading();
    const { email, password } = this.loginForm.value;
    this.auth
      .logearUsuario(email, password)
      .then((credenciales) => {
        Swal.close();
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Has sido logeado',
          showConfirmButton: false,
          timer: 1500,
        });
        this.router.navigate(['/']);
      })
      .catch((error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error.message,
        });
      });
  }

  loading() {
    Swal.fire({
      title: 'Espere...',
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }
}
