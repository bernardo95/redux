import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {map} from 'rxjs/operators';
import { Usuario } from '../models/usuario.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public auth:AngularFireAuth,public firestore:AngularFirestore) { 
  }
  initAuthListener(){
    this.auth.authState.subscribe(user=>{
      console.log(user);
      console.log(user?.uid);
      console.log(user?.email);
    });
  }

  crearUsuario(nombre:string,email:string, password:string){
    return this.auth.createUserWithEmailAndPassword(email,password).then(fuser=>{
      const {uid , email} = fuser.user;
      const newUser = new Usuario(uid, nombre, email);
      return this.firestore.doc(`${uid}/usuario`).set({...newUser});
    });
  }

  logearUsuario(email:string , password: string){
    return this.auth.signInWithEmailAndPassword(email,password);
  }
  logout(){
    return this.auth.signOut();
  }

  isAuth(){
    return this.auth.authState.pipe(
      map(fuser=> fuser != null)
    );
  }
}
