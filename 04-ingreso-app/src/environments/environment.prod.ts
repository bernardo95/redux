export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyD5f4480aiXffGJ2RH_SAvyq1e74gcCQnw',
    authDomain: 'ingresos-egresos-app-b077b.firebaseapp.com',
    databaseURL: 'https://ingresos-egresos-app-b077b.firebaseio.com',
    projectId: 'ingresos-egresos-app-b077b',
    storageBucket: 'ingresos-egresos-app-b077b.appspot.com',
    messagingSenderId: '471338947075',
    appId: '1:471338947075:web:feca913829b506ef35b436'
  }
};