// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD5f4480aiXffGJ2RH_SAvyq1e74gcCQnw',
    authDomain: 'ingresos-egresos-app-b077b.firebaseapp.com',
    databaseURL: 'https://ingresos-egresos-app-b077b.firebaseio.com',
    projectId: 'ingresos-egresos-app-b077b',
    storageBucket: 'ingresos-egresos-app-b077b.appspot.com',
    messagingSenderId: '471338947075',
    appId: '1:471338947075:web:feca913829b506ef35b436'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
