import { Action } from "../ngrx/ngrx";

export const incrementatorAction: Action = {
    type: "INCREMENTAR",
  };
  
  export const decrementadorAction: Action = {
      type: "DECREMENTAR",
  };
  
  export const multiplicarAction: Action = {
    type: "MULTIPLICAR",
    payload: 2
  };
  export const dividirrAction: Action = {
    type: "DIVIDIR",
    payload: 2
  };
  export const resetAction: Action = {
    type: "RESET",
  };