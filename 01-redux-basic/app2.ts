import { Action } from "./ngrx/ngrx";
import {
  incrementatorAction,
  decrementadorAction,
  dividirrAction,
  multiplicarAction,
  resetAction
} from "./contador/contador.actions";
import { reducer } from "./contador/contador.reducer";



console.log(reducer(10, incrementatorAction));
console.log(reducer(10, decrementadorAction));
console.log(reducer(10, multiplicarAction));
console.log(reducer(10, dividirrAction));
console.log(reducer(10, resetAction));
