// import { reducer } from './contador/contador.reducer';
import { Reducer, Action } from './ngrx/ngrx';
import { contadorReducer } from './contador/contador.reducer';
import { incrementatorAction } from './contador/contador.actions';
class Store<T> {
    constructor(private reducer:Reducer<T>, private state: T){
    
    }

    getState(){
        return this.state;
    }

    dispatch(action:Action){
        this.state = this.reducer(this.state,action);
    }
}

const store = new Store(contadorReducer, 10);

console.log(store.getState());

store.dispatch(incrementatorAction);
console.log(store.getState());

