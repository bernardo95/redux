//Acciones

import {Action} from './ngrx/ngrx';
import {incrementatorAction,decrementadorAction,dividirrAction,multiplicarAction} from './contador/contador.actions';

function reducer(state = 10, action: Action) {
  switch (action.type) {
    case "INCREMENTAR":
      return (state += 1);
      break;
    case "DECREMENTAR":
      return (state -= 1);
      break;
    case "MULTIPLICAR":
      return (state * action.payload);
      break;
    case "DIVIDIR":
      return (state / action.payload);
      break;
    default:
      return state;
      break;
  }
}


// console.log(reducer(10, incrementatorAction));
console.log(reducer(10, dividirrAction));

