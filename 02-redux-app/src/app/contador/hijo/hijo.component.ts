import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.redycers';
import * as actions from '../../contador/contador.actions';


@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {
  contador:number;

  constructor(private store:Store<AppState>) { 
  }

  ngOnInit(): void {
    this.store.select('count').subscribe(count => this.contador = count);

  }

  multiplicar(){
    this.store.dispatch(actions.multiply({number: 3}));
  }
  dividir(){
    this.store.dispatch(actions.divider({number: 5}));
  }

}
