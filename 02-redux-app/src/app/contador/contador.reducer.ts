import { createReducer, on } from "@ngrx/store";
import { increment, decrement, multiply, reset } from './contador.actions';

export const initialState = 0;

const _counterReducer = createReducer(
  initialState,
  on(increment, (state) => state + 1),
  on(decrement, (state) => state - 1),
  on(multiply, (state, {number}) => state * number),
  on(multiply, (state, {number}) => state / number),
  on(reset, (state) => 0),
);

export function counterRecuder(state, action) {
  return _counterReducer(state, action);
}

// export function countReducer(state:number = 10, action:Action){
//     switch(action.type){
//         case increment.type:
//             return state + 1;
//         case decrement.type:
//             return state - 1;
//         default:
//             return state;
//     }
// }
