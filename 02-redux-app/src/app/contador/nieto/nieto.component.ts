import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.redycers';
import { reset } from '../contador.actions';

@Component({
  selector: 'app-nieto',
  templateUrl: './nieto.component.html',
  styleUrls: ['./nieto.component.css']
})
export class NietoComponent implements OnInit {
  contador:number;
  constructor(private store:Store<AppState>) {
    this.store.select('count').subscribe(count => this.contador = count);
   }

  ngOnInit(): void {
  }
  reset(){
    this.store.dispatch(reset());
  }

}
